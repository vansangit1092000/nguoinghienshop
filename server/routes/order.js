const express = require('express');
const router = express.Router();
const Order = require('../models/Order');

/// @route GET api/orders
/// @desc Get all orders
/// @access Public
router.get('/', async(req, res) => {
    try {
        const orders = await Order.find();
        res.json(orders);
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting orders" });
    }
})


/// @route GET api/orders/:id
/// @desc Get order by id
/// @access Public
router.get('/:id', async(req, res) => {
    try {
        const order = await Order.findById(req.params.id);
        if (!order)
            return res.status(400).send('Order not found');
        res.json(order);
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting order" });
    }
})


/// @route POST api/orders
/// @desc Create new order
/// @access Public
router.post('/', async(req, res) => {
    try {
        const order = new Order({
            user: req.body.user,
            products: req.body.products,
            total: req.body.total,
            date: req.body.date,
            status: req.body.status,
        });
        await order.save();
        res.json({
            success: true,
            order: order
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error creating order" });
    }
})


/// @route PUT api/orders/:id
/// @desc Update order by id
router.put('/:id', async(req, res) => {
    try {
        const order = await Order.findById(req.params.id);
        if (!order)
            return res.status(400).send('Order not found');
        order.user = req.body.user;
        order.products = req.body.products;
        order.total = req.body.total;
        order.date = req.body.date;
        order.status = req.body.status;
        await order.save();
        res.json({
            success: true,
            order: order
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error updating order" });
    }
})


/// @route DELETE api/orders/:id
/// @desc Delete order by id
router.delete('/:id', async(req, res) => {
    try {
        await Order.findByIdAndDelete(req.params.id);
        res.json({
            success: true,
            message: "Order deleted"
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error deleting order" });
    }
})