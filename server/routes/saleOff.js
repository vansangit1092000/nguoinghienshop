const express = require('express');
const router = express.Router();
const SaleOff = require('../models/saleOff');

/// @route GET api/saleOffs
/// @desc Get all saleOffs
/// @access Public
router.get('/', async(req, res) => {
    try {
        const saleOffs = await SaleOff.find();
        res.json(saleOffs);
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting saleOffs" });
    }
})

/// @route GET api/saleOffs/:id
/// @desc Get saleOff by id
/// @access Public
router.get('/:id', async(req, res) => {
    try {
        const saleOff = await SaleOff.findById(req.params.id);
        if (!saleOff)
            return res.status(400).send('saleOff not found');
        res.json(saleOff);
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting saleOff" });
    }
})


/// @route POST api/saleOffs
/// @desc Create new saleOff
/// @access Public
router.post('/', async(req, res) => {
    try {
        const saleOff = new SaleOff({
            name: req.body.name,
            description: req.body.description,
            startDate: req.body.startDate,
            endDate: req.body.endDate,
            discount: req.body.discount,
        });
        await saleOff.save();
        res.json({
            success: true,
            saleOff: saleOff
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error creating saleOff" });
    }
})


/// @route PUT api/saleOffs/:id
/// @desc Update saleOff by id
router.put('/:id', async(req, res) => {
    try {
        const saleOff = await SaleOff.findById(req.params.id);
        if (!saleOff)
            return res.status(400).send('saleOff not found');
        saleOff.name = req.body.name;
        saleOff.description = req.body.description;
        saleOff.startDate = req.body.startDate;
        saleOff.endDate = req.body.endDate;
        saleOff.discount = req.body.discount;
        await saleOff.save();
        res.json({
            success: true,
            saleOff: saleOff
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error updating saleOff" });
    }
})


/// @route DELETE api/saleOffs/:id
/// @desc Delete saleOff by id
router.delete('/:id', async(req, res) => {
    try {
        const saleOff = await SaleOff.findById(req.params.id);
        if (!saleOff)
            return res.status(400).send('saleOff not found');
        await saleOff.remove();
        res.json({
            success: true,
            saleOff: saleOff
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error deleting saleOff" });
    }
})


/// @route GET api/saleOffs/:id
/// @desc Get saleOff by id
/// @access Public
router.get('/:id', async(req, res) => {
    try {
        const saleOff = await SaleOff.findById(req.params.id);
        if (!saleOff)
            return res.status(400).send('saleOff not found');
        res.json(saleOff);
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting saleOff" });
    }
})