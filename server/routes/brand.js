const express = require('express');
const router = express.Router();
const Brand = require('../models/Brand');

/// @route GET api/brands
/// @desc Get all brands
/// @access Public
router.get('/', async(req, res) => {
    try {
        const brands = await Brand.find();
        res.json(brands);
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting brands" });
    }
})


/// @route GET api/brands/:id
/// @desc Get brand by id
/// @access Public
router.get('/:id', async(req, res) => {
    try {
        const brand = await Brand.findById(req.params.id);
        if (!brand)
            return res.status(400).send('Brand not found');
        res.json(brand);
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting brand" });
    }
})


/// @route POST api/brands
/// @desc Create new brand
/// @access Public
router.post('/', async(req, res) => {
    try {
        const brand = new Brand({
            name: req.body.name,
            description: req.body.description,
        });
        await brand.save();
        res.json({
            success: true,
            brand: brand
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error creating brand" });
    }
})


/// @route PUT api/brands/:id
/// @desc Update brand by id
router.put('/:id', async(req, res) => {
    try {
        const brand = await Brand.findById(req.params.id);
        if (!brand)
            return res.status(400).send('Brand not found');
        brand.name = req.body.name;
        brand.image = req.body.image;
        brand.description = req.body.description;
        brand.createAt = req.body.createAt;
        await brand.save();
        res.json({
            success: true,
            brand: brand
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error updating brand" });
    }
})


/// @route DELETE api/brands/:id
/// @desc Delete brand by id
router.delete('/:id', async(req, res) => {
    try {
        const brand = await Brand.findById(req.params.id);
        if (!brand)
            return res.status(400).send('Brand not found');
        await brand.remove();
        res.json({
            success: true,
            brand: brand
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error deleting brand" });
    }
})


module.exports = router;