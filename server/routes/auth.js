const express = require('express');
const router = express.Router();
const User = require('../models/User');
const argon2 = require('argon2');
const jwt = require('jsonwebtoken');


// @route POST api/users/register
// @desc Register user
// @access Public
router.post('/register', async(req, res) => {
    const {
        username,
        password,
        email,
        dateOfBirth,
        address,
        phoneNumber,
        role,
        wishlist
    } = req.body;

    if (!username || !password) {
        return res.status(400).send('Username and password are required');
    }

    try {
        const user = await User.findOne({ username });

        if (user)
            return res.status(400).send('Username already exists');

        const hasedPassword = await argon2.hash(password, 12);

        const newUser = new User({
            username,
            password: hasedPassword,
            email,
            dateOfBirth,
            address,
            phoneNumber,
            role,
            wishlist
        });

        await newUser.save();

        const accessToken = jwt.sign({ userId: newUser._id }, process.env.ACCESS_TOKEN_SECRET);

        res.json({
            success: true,
            message: "User created successfully",
            accessToken: accessToken
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error registering user" });
    }
})

/// @route POST api/users/login
/// @desc Login user and return JWT token
/// @access Public
router.post('/login', async(req, res) => {
    const { username, password } = req.body;
    if (!username || !password)
        return res.status(400).send('Username and password are required');
    try {
        const user = await User.findOne({ username });
        if (!user)
            return res.status(400).send('Username or password is incorrect');
        const isMatch = await argon2.verify(user.password, password);
        if (!isMatch)
            return res.status(400).send('Username or password is incorrect');
        const accessToken = jwt.sign({ userId: user._id }, process.env.ACCESS_TOKEN_SECRET);
        res.json({
            success: true,
            message: "User logged in successfully",
            accessToken: accessToken
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error logging in user" });
    }
})


/// @route GET api/users/:id
/// @desc Get user data
/// @access Public
router.get('/:id', async(req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user)

            return res.status(400).send('User not found');
        res.json({
            success: true,
            user: user
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting user data" });
    }
})

/// @route PUT api/users/:id
/// @desc Update user data
/// @access Public
router.put('/:id', async(req, res) => {
    const {
        username,
        password,
        email,
        dateOfBirth,
        address,
        phoneNumber,
        role,
        wishlist
    } = req.body;
    try {
        const user = await User.findById(req.params.id);
        if (!user)
            return res.status(400).send('User not found');
        if (username)
            user.username = username;
        if (password)
            user.password = await argon2.hash(password, 12);
        if (email)
            user.email = email;
        if (dateOfBirth)
            user.dateOfBirth = dateOfBirth;
        if (address)
            user.address = address;
        if (phoneNumber)
            user.phoneNumber = phoneNumber;
        if (role)
            user.role = role;
        if (wishlist)
            user.wishlist = wishlist;
        await user.save();
        res.json({
            success: true,
            user: user
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error updating user data" });
    }
})

/// @route DELETE api/users/:id
/// @desc Delete user
/// @access Public
router.delete('/:id', async(req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user)

            return res.status(400).send('User not found');
        await user.remove();
        res.json({
            success: true,
            message: "User deleted successfully"
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error deleting user" });
    }
})

/// @route GET api/users/:id/wishlist
/// @desc Get user wishlist
/// @access Public
router.get('/:id/wishlist', async(req, res) => {
    try {
        const user = await User.findById(req.params.id);
        if (!user)
            return res.status(400).send('User not found');
        res.json({
            success: true,
            wishlist: user.wishlist
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting user wishlist" });
    }
})


module.exports = router;