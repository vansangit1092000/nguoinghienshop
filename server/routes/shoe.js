const express = require('express');
const router = express.Router();
const Shoe = require('../models/Shoe');

/// @route GET api/shoes
/// @desc Get all shoes
/// @access Public
router.get('/', async(req, res) => {
    try {
        const shoes = await Shoe.find();
        res.json(shoes);
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting shoes" });
    }
})


/// @route GET api/shoes/:id
/// @desc Get shoe by id
/// @access Public
router.get('/:id', async(req, res) => {
    try {
        const shoe = await Shoe.findById(req.params.id);
        if (!shoe)
            return res.status(400).send('Shoe not found');
        res.json(shoe);
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error getting shoe" });
    }
})


/// @route POST api/shoes
/// @desc Create new shoe
/// @access Public
router.post('/', async(req, res) => {
    try {
        const shoe = new Shoe({
            name: req.body.name,
            price: req.body.price,
            image: req.body.image,
            description: req.body.description,
            category: req.body.category,
            size: req.body.size,
            color: req.body.color,
            brand: req.body.brand,
            quantity: req.body.quantity,
            createAt: req.body.createAt
        });
        await shoe.save();
        res.json({
            success: true,
            shoe: shoe
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error creating shoe" });
    }
})


/// @route PUT api/shoes/:id
/// @desc Update shoe by id
/// @access Public
router.put('/:id', async(req, res) => {
    const {
        name,
        price,
        image,
        description,
        category,
        size,
        color,
        brand,
        quantity,
    } = req.body;
    try {
        const shoe = await Shoe.findById(req.params.id);
        if (!shoe)
            return res.status(400).send('Shoe not found');
        if (name)
            shoe.name = name;
        if (price)
            shoe.price = price;
        if (image)
            shoe.image = image;
        if (description)
            shoe.description = description;
        if (category)
            shoe.category = category;
        if (size)
            shoe.size = size;
        if (color)
            shoe.color = color;
        if (brand)
            shoe.brand = brand;
        if (quantity)
            shoe.quantity = quantity;
        await shoe.save();
        res.json({
            success: true,
            shoe: shoe
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error updating shoe" });
    }
})


/// @route DELETE api/shoes/:id
/// @desc Delete shoe by id
/// @access Public
router.delete('/:id', async(req, res) => {
    try {
        const shoe = await Shoe.findById(req.params.id);
        if (!shoe)
            return res.status(400).send('Shoe not found');
        await shoe.remove();
        res.json({
            success: true,
            shoe: shoe
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({ success: false, message: "Error deleting shoe" });
    }
})


module.exports = router;