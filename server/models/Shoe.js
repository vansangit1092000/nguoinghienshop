const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ShoeSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    price: {
        type: Number,
        required: true
    },
    quantityForSize: [{
        size: {
            type: Number,
            required: true
        },
        quantity: {
            type: Number,
            required: true
        }
    }],
    desription: {
        type: String,
    },
    images: {
        type: Array,
        required: true
    },
    brand: {
        type: String,
    },
    status: {
        type: String,
        enum: ['available', 'sold out', 'incoming'],
        default: 'available'
    },
    saleOff: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'SaleOff'
    }
});

module.exports = mongoose.model('Shoe', ShoeSchema);