const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SaleOffSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    discount: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('SaleOff', SaleOffSchema);