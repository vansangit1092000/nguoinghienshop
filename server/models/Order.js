const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    orderDate: {
        type: Date,
        default: Date.now
    },
    orderStatus: {
        type: String,
        enum: ['paid', 'pending', 'cancel'],
        default: 'pending'
    },
    orderAddress: {
        type: String,
        required: true
    },
    orderPayment: {
        type: String,
        enum: ['COD', 'Momo'],
        default: 'COD'
    },
    orderDetails: [{
        shoe: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Shoe'
        },
        quantity: {
            type: Number,
            required: true
        },
        price: {
            type: Number,
            required: true
        }
    }],
    orderTotal: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Order', OrderSchema);