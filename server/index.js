require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const authRouter = require('./routes/auth');
const brandRouter = require('./routes/brand');


const connectDB = async() => {
    try {
        await mongoose.connect(`mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@fake-you.neypm.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('MongoDB Connected...');
    } catch (err) {
        console.error(err.message);
        process.exit(1);
    }
}

connectDB();

const app = express();

app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/brand', brandRouter);

const PORT = process.env.DB_PORT || 5000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));